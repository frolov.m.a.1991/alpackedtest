FROM ubuntu:16.04
MAINTAINER Maxim Frolov <frolov.m.a.1991@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive
ENV MYSQL_USER=root
ENV MYSQL_PASS=

WORKDIR /tmp
#install some packages for zabbix (server, frontend, agent) and database
RUN apt-get update && apt-get -y --no-install-recommends install mysql-server apache2 libapache2-mod-php php php-mbstring php-gd php-xml php-bcmath php-ldap php-mysql wget ca-certificates
#install zabbix
RUN wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+xenial_all.deb && \
	dpkg -i zabbix-release_4.0-3+xenial_all.deb && \
	apt-get -y update && \
	apt-get -y install zabbix-server-mysql zabbix-frontend-php zabbix-agent && \
	apt-get -y -f install
#some changes in db for zabbix-server work
RUN touch /tmp/create.sql && \
	echo "create database zabbix character set utf8 collate utf8_bin; grant all privileges on zabbix.* to zabbix@localhost identified by '123';" > /tmp/create.sql

RUN service mysql start && \
	mysql --user=root --password="" < /tmp/create.sql && cd /usr/share/doc/zabbix-server-mysql && zcat create.sql.gz | mysql --user=root --password="" zabbix
#copy configuration file for zabbix-server and shell script for running services after build of container
COPY ./config/zabbix_server.conf .
COPY start.sh .
#kill warning in apache 
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN cp /tmp/zabbix_server.conf /etc/zabbix/zabbix_server.conf
RUN chmod +x /tmp/start.sh
#changes in timezone for fronend zabbix-server
RUN echo "date.timezone = 'Europe/Kiev'" >> /etc/php/7.0/apache2/php.ini

WORKDIR /
#ports for server and frontend
EXPOSE 80/tcp 10051/TCP 10050/TCP

ENTRYPOINT ["/tmp/start.sh", "-d"]

CMD ["tail", "-f", "/dev/null"]