#!/bin/sh

### starting services at start container
service mysql start
service apache2 start
service zabbix-server start
service zabbix-agent start

### for "daemon" mode by container
if [ "$1" = "-d" ]
 then
  while true
   do sleep 1000
  done
fi

### bash command line to debug
if [ "$1" = "-bash" ]
 then
  /bin/bash
fi